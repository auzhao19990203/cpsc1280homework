cut -d"," -f7 $1 > temp1
cut -d"," -f6 $1 > temp2
cut -d"," -f5 $1 > temp3
cut -d"," -f4 $1 > temp4
cut -d"," -f3 $1 > temp5
a=`paste -d"," temp1 temp2 temp3 temp4 temp5 | tail -n +2 | sort -t"," -k1 | tee temp.csv | cut -d"," -f3 | uniq | wc -l`
b=`cat temp.csv`
echo "Total number of bus stops: $a"$'\n'$b
rm temp1 temp2 temp3 temp4 temp5 temp.csv