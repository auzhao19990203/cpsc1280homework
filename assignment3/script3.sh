rmdir $2
mkdir $2
find $1 -type d >dirs.txt
cp dirs.txt $2
cd $2
xargs mkdir -p <dirs.txt
rm dirs.txt
cd ..
rm dirs.txt
find $1 -type f -print | xargs ln   q3/ .bak