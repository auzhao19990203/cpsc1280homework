rdt=($(cat $2 | tail -n+1))
numRdt=${#rdt[@]}
cat $1 | while read line
do
    a=0
    loop=$line
    for j in $(seq 0 $(expr $numRdt - 1))
    do
        a=$(expr $a + $(echo $line | grep -c "${rdt[j]}"))
        if [ $a -gt 1 ]
        then
            echo $line | sed "s/./-/g"
            break
        else
            loop=$(echo $loop | sed "s/${rdt[j]}/----/")
        fi
        echo $loop
    done
done