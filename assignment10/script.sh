a=$(ping -c10 $1 | tee temp.csv | wc -m)
if [ $a -eq 0 ]
then
    echo "$1: Not Reachable" >output.csv
    exit 1
else
    echo "The server address: $1" >output.csv
    echo "the average time for upto 10 ping attempts: $(sed -n "/rtt/p" temp.csv | cut -d"/" -f5)ms" >>output.csv
    echo "the ttl of the first ping attempt: $(cat temp.csv | head -n2 | tail -n1 | cut -d" " -f6 | cut -d"=" -f2)" >>output.csv
    p=$(sed -n "/[[:digit:]]%/p" temp.csv | cut -d" " -f6 | sed "s/%//")
    f=$(echo "10*$(echo "$p/100" | bc)" | bc)
    echo "number of failed ping attemps: $f" >>output.csv
    echo "number of successful ping attempts: $(expr 10 - $f)" >>output.csv
    echo "The time in which the ping was initiated: $(sed -n "/time /p" temp.csv | cut -d" " -f10)" >>output.csv
fi
rm temp.csv